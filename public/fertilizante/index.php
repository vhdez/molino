<?php

function calculaFertilizanteAUtilizar($parcelaAltura, $parcelaAnchura)
{
    $calculaReglaDeTres = function ($a, $b, $c) {
        return ($b * $c) / $a;
    };

    $parcelaMetrosCuadrados = $parcelaAltura * $parcelaAnchura;

    $kilosDeMaizAUtilizar = $calculaReglaDeTres(1.8, 2 / 3, $parcelaMetrosCuadrados);
    $litrosDeAguaAUtilizar = $calculaReglaDeTres(1.5, 3, $kilosDeMaizAUtilizar);
    $fertilizanteAUtilizar = $calculaReglaDeTres(2.5, 0.75, $litrosDeAguaAUtilizar);

    return round($fertilizanteAUtilizar, 2);
}

$input = fgets(STDIN);
$vars = explode(' ', $input);

echo calculaFertilizanteAUtilizar($vars[0], $vars[1]);
