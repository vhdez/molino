<?php

function numeroTarjetaValido($numeroTarjeta)
{
    if (strlen($numeroTarjeta) != 16) {
        return 'NO valida';
    }

    $primerosDigitos = str_split(substr($numeroTarjeta . '', 0, 15));
    $ultimoDigito = substr($numeroTarjeta . '', 15, 1);

    $suma = 0;
    foreach ($primerosDigitos as $i => $digito) {
        if (($i + 1) % 2 == 0) {
            $suma += $digito;
        } else {
            $multiplicacion = $digito * 2;

            if ($multiplicacion > 9) {
                $suma += array_sum(str_split($multiplicacion . ''));
            } else {
                $suma += $multiplicacion;
            }
        }
    }

    $verificacion = ($suma + $ultimoDigito) / 10;
    return floor($verificacion) == $verificacion ? 'VALIDA' : 'NO valida';
}

$input = '';
while ($f = fgets(STDIN)) {
    $input .= $f;
}

$lineas = explode(PHP_EOL, $input);
$n = array_shift($lineas);

if (($n < 0 || $n > 15)) {
    echo 'INCORRECTO';
    die();
}

$output = '';
foreach ($lineas as $i => $linea) {
    if (!empty($linea)) {
        $output .= numeroTarjetaValido($linea);
    }

    if ($i < count($lineas) - 1) {
        $output .= PHP_EOL;
    }
}

echo $output;
