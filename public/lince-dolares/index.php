<?php

function calculadoraLinceDolares($depositoInicial, $tasaCrecimiento, $montoDeseado)
{
    if ($depositoInicial <= 0 || ($tasaCrecimiento <= 0 || $tasaCrecimiento > 100) || $montoDeseado <= 0) {
        return -1;
    }

    $years = 1;
    $montoBase = $depositoInicial;
    do {
        $montoCalculado = $montoBase + ($montoBase * ($tasaCrecimiento / 100) * $years);

        if ($montoCalculado >= $montoDeseado) {
            return $years;
        }

        $montoBase = $montoCalculado;
        $years++;
    } while (true);
}

$input = '';
while ($f = fgets(STDIN)) {
    $input .= $f;
}

$lineas = explode(PHP_EOL, $input);
array_shift($lineas);

$output = '';
foreach ($lineas as $i => $linea) {
    if (!empty($linea)) {
        $vars = explode(' ', $linea);

        $output .= calculadoraLinceDolares($vars[0], $vars[1], $vars[2]);
    }

    if ($i < count($lineas) - 1) {
        $output .= PHP_EOL;
    }
}

echo $output;

