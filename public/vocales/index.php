<?php

function cuentaGruposDeVocales($texto)
{
    $caracteres = str_split($texto);

    $vocales = ['a', 'e', 'i', 'o', 'u'];
    $total = 0;
    $enGrupo = false;
    foreach ($caracteres as $caracter) {
        $esVocal = in_array(strtolower($caracter), $vocales);

        if ($esVocal && !$enGrupo) {
            $total++;
            $enGrupo = true;
        }

        if (!$esVocal) {
            $enGrupo = false;
        }
    }
    return $total;
}

$input = '';
while ($f = fgets(STDIN)) {
    $input .= $f;
}

$lineas = explode(PHP_EOL, $input);

$output = '';
foreach ($lineas as $i => $linea) {
    if (!empty($linea)) {
        $output .= cuentaGruposDeVocales($linea);
    }

    if ($i < count($lineas) - 1) {
        $output .= PHP_EOL;
    }
}

echo $output;

